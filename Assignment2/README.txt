For my project, I have created a game called "Dr. Drew's City Escape"
The premise of the game is simple, you draw shapes intro the environment
to navigate moving platforms and avoid the ground below, which would mean your
doom. The in game instructions should be sufficient for actually playing the
game.

Creativity:
The game concept is novel, the complex character sprite was produced using
unique, creative, and novel 3D shapes I produced, as were the recursively
constructed trees used to create Dr. Drew's bridges and stairs.

Complexity:
The game is very complex, its complexity coming in three main flavors:
1. The custom shapes (hexagonal cylinder, 'moustache' cylinders, eraser
cap)
2. Complex drawing functions, especially my Tree shape, which made use
of L-systems and complex recursive calls to produce a 'natural' looking
tree
3. The colliders and object physics I built from the ground up (without use
of any of the template code for collisions as it wasn't available yet) to
implement landing on platforms, maintaining velocity of a platform while
on top of it (so you don't slide off), colliding with platforms if you
attempt to jump 'up' platforms rather than 'on top' of them, the Procedurally
generated platform placement, to name a few.

Hierarchy:
Most of my custom shapes were used in multi-level hierarchical structures,
including the pencil, which has several levels of nested hierarchical
structure, as well as the recursively drawn trees, which have 3 recursive
levels of hierarchy as defined by the draw calls. 

Overall Quality:
The objects and camera move fluidly and are all incremented using versions
of an explicit euler, where the dT increment comes from calls to
graphics_state.animation_delta_time to ensure that the dT increments are
consistent across devices. Excruciating attention detail was paid to the
collision and movement physics to ensure they worked in the exact manner
desired (bouncing off the side of objects, maintaining velocity of an
object you are on top of, etc.)

Camera Tracking:
Use the number buttons 1 through 6 (as explained in the game's tutorial) to
see the fluid movement between all the different camera angles, which are
left to the player to select so they can use whatever best suits their play
style. The camera also tracks the pencil character regardless of viewing angle,
which also required complex use of the look_at() function.

Design Polygonal Objects:
Objects Designed:

'octagon'        : new Octagon (8), //pencil body
'eraser'           : new Eraser_Top( 5, 15, [[0,1],[0,1]]), //pencil eraser
'flatoid'          : new Flat_Toriod(8,15, [[0,1],[0,1]]), //pencil eraser cap
'octacone'         : new Octagon_Cone(8), //pencil tip
'mustache'         : new Curved_Cylinder(5,15,[[0,1],[0,1]]), //pencil
 moustache and eyebrows
'tree3'           : new Tree(true, 3), //recursively drawn tree
'leaf3'           : new Tree(false, 3), //leaves for recursively drawn tree


Texturing:
All objects I created were assigned custom texture coordinates and Textures
witch great attention to detail to ensure they looked convincing

Real Time speed:
I ensured running at real time speed by using calls to
graphics_state.animation_delta_time

Frame Rate:
The frame rate of the program is displayed below the lower left hand corner
of the screen, next to the score readout
