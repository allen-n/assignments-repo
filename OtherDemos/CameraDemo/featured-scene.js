class Out_Of_Body_Demo extends Scene_Component
{ constructor( context )
    { super( context );
      this.submit_shapes( context, { arrows : new Axis_Arrows() } );
      Object.assign( this, { location: Mat4.identity(),
                                  rgb: context.get_instance( Phong_Model ).material( Color.of( 0,0,0,1 ),  1, 1, 1, 40, context.get_instance( "/assets/rgb.jpg" ) ),
                               purple: context.get_instance( Phong_Model ).material( Color.of( 1,0,1,1 ), .4, 1, 1, 40                                          ) } );
      Object.assign( context.globals.graphics_state, { camera_transform: Mat4.translation([ 0,-5,-25 ]), 
                                                   projection_transform: Mat4.perspective( Math.PI/4, context.width/context.height, .1, 1000 ) } );                                                  
      context.globals.graphics_state.lights = [ new Light( Vec.of( 1,1,0,0 ).normalized(), Color.of(  1, .5, .5, 1 ), 100000 ),
                                                new Light( Vec.of( 0,1,0,0 ).normalized(), Color.of( .5,  1, .5, 1 ), 100000 ) ];
      this.location = Mat4.identity();         
    }
  make_control_panel()   // This function of a scene sets up its keyboard shortcuts.
    { this.key_triggered_button( "Capture controls", "c", function() 
                                                          { this.globals.movement_target_is_a_camera = false;
                                                            let ref = this;
                                                            this.globals.movement_controls_target = function() { return ref.location }    
                                                          },  "red" ); this.new_line();
    }
  show_explanation( document_element )
    { document_element.innerHTML += "<p>This demo shows the difference between controlling a camera and controlling an object, using the same key controls "
                                 +  "for flying.  At first, the movement keys like \"w\" control the camera, but the red button below captures movement input "
                                 +  "and controls the middle axis arrows instead.  Press \"c\" and \"SHIFT+r\" to alternate between controlling one or the other "
                                 +  "with the other movement buttons below.  When the camera is what is being controlled, the Movement_Controls class applies the "
                                 +  "inverse matrices of what it normally would do to move a regular non-camera object the same way.</p><p>Remember that all "
                                 +  "actions upon the camera apply the matrix inverse of those actions to all the drawn shapes.  To invert a matrix "
                                 +  "product when several matrices are chained together, you must both invert the parts and also reverse their order.  "
                                 +  "For more explanation of where the camera matrix fits in to the final chain of transforms, check <a "
                                 +  "href='https://piazza.com/class_profile/get_resource/j855t03rsfv1cn/j9c4ruopm3h22z' target='blank'>this .pdf</a>. Here this "
                                 +  "final matrix multiplication including the camera matrix happens in the vertex shader code, found in the Phong_Model class.";
    }
  display( graphics_state )
    { this.shapes.arrows.draw( graphics_state, this.location, this.rgb );
      for( let i of [ -5, 5 ] ) for( let j of [ -5, 5 ] ) this.shapes.arrows.draw( graphics_state, Mat4.translation([ i,0,j ]), this.purple );      
    }
}